<?php

/*
@@ Save all setting tabs data
@@ When user will click on submit button
*/

add_action( 'wp_ajax_nopriv_opos_add_cart', 'opos_add_cart_save_options_setting_data' );
add_action( 'wp_ajax_opos_add_cart', 'opos_add_cart_save_options_setting_data' );
function opos_add_cart_save_options_setting_data(){
	$response_arr=array();
    $form_data=$_REQUEST;
	
	/*************************************************
	@@Store Latest Cart summary
	*******************************************************/
	unset($_SESSION['opos_latest_cart_items']);
	$latest_item_total=$form_data['product_item_price_japan']*$form_data['product_quantity'];
	$product_domestic_shipping='19.26';
	$product_service_fee='10.50';

	$_SESSION['opos_latest_cart_items']['Item Total']=$latest_item_total;
	$_SESSION['opos_latest_cart_items']['Domestic Shipping']=$product_domestic_shipping;
	$_SESSION['opos_latest_cart_items']['Minimum Service Fee']=$product_service_fee;
	$_SESSION['opos_latest_cart_items']['Estimated Total']=$latest_item_total+$product_domestic_shipping+$product_service_fee;
	
	$_SESSION['opos_cart_items'][] = $form_data;
	$html='';
	if(!empty($_SESSION['opos_cart_items'])){
		
		$opos_cart_page_id=get_option('opos_cart_page_id');
		$opos_checkout_page_id=get_option('opos_checkout_page_id');
		$response_arr['link']=get_permalink($opos_cart_page_id);
		$response_arr['status']=true;
	}
    echo json_encode($response_arr);
    wp_die();

}

/*
@@ Delete Items from cart
@@ When user will click on submit button
*/

add_action( 'wp_ajax_nopriv_opos_delete_cart', 'opos_delete_cart_options_setting_data' );
add_action( 'wp_ajax_opos_delete_cart', 'opos_delete_cart_options_setting_data' );
function opos_delete_cart_options_setting_data(){
	$form_data=$_REQUEST;
	if(!empty($form_data)){
		unset($_SESSION['opos_cart_items'][$form_data['item_id']]);
	}
	die;
}

/**************************************************
@@Login form function
@@ When user will click on submit button
******************************************************/

add_action( 'wp_ajax_nopriv_ops_user_login', 'ops_user_login_options_setting_data' );
add_action( 'wp_ajax_ops_user_login', 'ops_user_login_options_setting_data' );
function ops_user_login_options_setting_data(){
	   //Nonce is checked, get the POST data and sign user on
    $info = array();
        $info['user_login'] = $_POST['username'];
        $info['user_password'] = $_POST['password'];
        $info['remember'] = true;

    $user_signon = wp_signon( $info, false );
    if ( is_wp_error( $user_signon )) {
        echo json_encode( array( 'loggedin'=>false, 'message'=>__( '<span class="opos-errors">Wrong username or password!</span>' )));
    } else {
        echo json_encode( array( 'loggedin'=>true, 'message'=>__('<span class="opos-success">Login successful, redirecting...</span>' )));
    }

    die();
}

/**************************************************
@@Register form function
@@ When user will click on submit button
******************************************************/

add_action( 'wp_ajax_nopriv_ops_user_register', 'ops_user_register_options_setting_data' );
add_action( 'wp_ajax_ops_user_register', 'ops_user_register_options_setting_data' );
function ops_user_register_options_setting_data(){
	if (isset($_POST['username']))
	{
		global $reg_errors;
		$reg_errors = new WP_Error;
		$username=$_POST['username'];
		$useremail=$_POST['useremail'];
		$password=$_POST['password'];
		
		
		if(empty( $username ) || empty( $useremail ) || empty($password))
		{
			$reg_errors->add('field', 'Required form field is missing');
		}    
		if ( 6 > strlen( $username ) )
		{
			$reg_errors->add('username_length', 'Username too short. At least 6 characters is required' );
		}
		if ( username_exists( $username ) )
		{
			$reg_errors->add('user_name', 'The username you entered already exists!');
		}
		if ( ! validate_username( $username ) )
		{
			$reg_errors->add( 'username_invalid', 'The username you entered is not valid!' );
		}
		if ( !is_email( $useremail ) )
		{
			$reg_errors->add( 'email_invalid', 'Email id is not valid!' );
		}
		
		if ( email_exists( $useremail ) )
		{
			$reg_errors->add( 'email', 'Email Already exist!' );
		}
		if ( 5 > strlen( $password ) ) {
			$reg_errors->add( 'password', 'Password length must be greater than 5!' );
		}
		$signUpError='';
		if (is_wp_error( $reg_errors ))
		{ 
			foreach ( $reg_errors->get_error_messages() as $error )
			{
				 $signUpError='<p style="color:#FF0000; text-aling:left;opos-full-width"><strong>ERROR</strong>: '.$error . '<br /></p>';
			} 
		}
		
		
		if ( 1 > count( $reg_errors->get_error_messages() ) )
		{
			// sanitize user form input
			global $username, $useremail;
			$username   =   sanitize_user( $_POST['username'] );
			$useremail  =   sanitize_email( $_POST['useremail'] );
			$password   =   esc_attr( $_POST['password'] );
			
			$userdata = array(
				'user_login'    =>   $username,
				'user_email'    =>   $useremail,
				'user_pass'     =>   $password,
				);
			$user_id = wp_insert_user( $userdata );
			// Set the global user object
			$current_user = get_user_by( 'id', $user_id );

			// set the WP login cookie
			$secure_cookie = is_ssl() ? true : false;
			wp_set_auth_cookie( $user_id, true, $secure_cookie );
			echo json_encode( array( 'loggedin'=>true, 'message'=>__('<span class="opos-success">Register successful, redirecting...</span>' )));
		}
		else{
			 echo json_encode( array( 'loggedin'=>false, 'message'=>__( '<span class="opos-errors">'.$signUpError.'</span>' )));
			
		}

	}

    die();
}

/**************************************************
@@User Profile update form
@@ When user will click on submit button
******************************************************/

add_action( 'wp_ajax_nopriv_ops_user_profile_update', 'ops_user_profile_update' );
add_action( 'wp_ajax_ops_user_profile_update', 'ops_user_profile_update' );
function ops_user_profile_update(){
	$user = wp_get_current_user();
	if (isset($_POST['username']))
	{
		global $reg_errors;
		$username=$_POST['username'];
		$useremail=$_POST['useremail'];
		$userwebsite=$_POST['userwebsite'];
		
		$user_data = wp_update_user( array( 'ID' => $user->ID, 'user_url' => $userwebsite, 'user_email' => $useremail) );
		if ( is_wp_error( $user_data ) ) {
			echo json_encode( array( 'loggedin'=>false, 'message'=>__( '<span class="opos-errors">'.$user_data->get_error_message().'</span>' )));
		}
		else {
			echo json_encode( array( 'loggedin'=>false, 'message'=>__('<span class="opos-success">Profile has been updated</span>' )));
		}
		
	}

    die();
}

/**************************************************
@@User Shipping data
@@ When user will click on submit button
******************************************************/

add_action( 'wp_ajax_nopriv_ops_user_shipping_update', 'ops_user_shipping_update' );
add_action( 'wp_ajax_ops_user_shipping_update', 'ops_user_shipping_update' );
function ops_user_shipping_update(){
	$user = wp_get_current_user();
	if (!empty($_POST))
	{
		global $reg_errors;
		foreach($_POST as $user_meta_key =>  $meta_value){
			
			if(!empty($meta_value))
				update_user_meta( $user->ID, $user_meta_key,$meta_value);
		}
		
		echo json_encode( array( 'loggedin'=>false, 'message'=>__('<span class="opos-success">Profile has been updated</span>' )));
	}
	else {
		echo json_encode( array( 'loggedin'=>false, 'message'=>__('<span class="opos-success">Please fill any filed to save data</span>' )));
	}
		
	

    die();
}

/**************************************************
@@User Password data
@@ When user will click on submit button
******************************************************/

add_action( 'wp_ajax_nopriv_ops_user_password_update', 'ops_user_password_update' );
add_action( 'wp_ajax_ops_user_password_update', 'ops_user_password_update' );
function ops_user_password_update(){
	$user = wp_get_current_user();
	if (isset($_POST['current_password']) && isset($_POST['new_password']) && isset($_POST['confirm_password']))
	{
			global $reg_errors;
		$reg_errors = new WP_Error;
		
		$current_password=$_POST['current_password'];
		$new_password=$_POST['new_password'];
		$confirm_password=$_POST['confirm_password'];
		
		if ( isset( $_POST['new_password'] ) && ($_POST['new_password'] != $_POST['confirm_password']))
			$reg_errors->add( 'new_password_invalid', 'The passwords do not match' );
		
	
		 $signUpError='';
		if (is_wp_error( $reg_errors ))
		{ 
			foreach ( $reg_errors->get_error_messages() as $error )
			{
				 $signUpError='<p style="color:#FF0000; text-aling:left;opos-full-width"><strong>ERROR</strong>: '.$error . '<br /></p>';
			} 
		}
		if(!empty($signUpError)){
			echo json_encode( array( 'loggedin'=>false, 'message'=>__( '<span class="opos-errors">'.$signUpError.'</span>' )));
		}
		else{
			$user_data =wp_set_password( $new_password,  $user->ID );
			
			if ( is_wp_error( $user_data ) ) {
				echo json_encode( array( 'loggedin'=>false, 'message'=>__( '<span class="opos-errors">'.$user_data->get_error_message().'</span>' )));
			}
			else {
				echo json_encode( array( 'loggedin'=>false, 'message'=>__('<span class="opos-success">Profile has been updated</span>' )));
			}
		}
		
	}

    die();
		

}