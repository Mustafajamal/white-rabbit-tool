<?php

/*
@@ Save all setting tabs data
@@ When user will click on submit button
*/

add_action( 'wp_ajax_nopriv_opos_add_cart', 'opos_add_cart_save_options_setting_data' );
add_action( 'wp_ajax_opos_add_cart', 'opos_add_cart_save_options_setting_data' );
function opos_add_cart_save_options_setting_data(){
	$response_arr=array();
    $form_data=$_REQUEST;
	
	/*************************************************
	@@Store Latest Cart summary
	*******************************************************/
	unset($_SESSION['opos_latest_cart_items']);
	$latest_item_total=$form_data['product_item_price_japan']*$form_data['product_quantity'];
	$product_domestic_shipping='19.26';
	$product_service_fee='10.50';

	$_SESSION['opos_latest_cart_items']['Item Total']=$latest_item_total;
	$_SESSION['opos_latest_cart_items']['Domestic Shipping']=$product_domestic_shipping;
	$_SESSION['opos_latest_cart_items']['Minimum Service Fee']=$product_service_fee;
	$_SESSION['opos_latest_cart_items']['Estimated Total']=$latest_item_total+$product_domestic_shipping+$product_service_fee;
	
	$_SESSION['opos_cart_items'][] = $form_data;
	$html='';
	if(!empty($_SESSION['opos_cart_items'])){
		
		// foreach($_SESSION['opos_cart_items'] as $key_index => $single_item){
			// $item_total=$single_item['product_item_price_japan']*$single_item['product_quantity'];
			// $html.='
			// <tr class="parent-'.$key_index.'">
				// <td data-val="'.$single_item['product_url'].'" data-field="product_url">'.$single_item['product_url'].'</td>
				// <td data-val="'.$single_item['product_size_color'].'" data-field="product_size_color">'.$single_item['product_size_color'].'</td>
				// <td><div class="wre-product-view-cart-image-preview"><span class="font-trans">No image found.</span></div></td>
				// <td data-val="'.$single_item['product_quantity'].'" data-field="product_quantity">'.$single_item['product_quantity'].'</td>
				// <td data-val="'.$single_item['product_item_price_japan'].'" data-field="product_item_price_japan">¥'.$single_item['product_item_price_japan'].'<small class="opos-full-width">($0.11)</small></td>
				// <td data-val="'.$item_total.'" data-field="item_total">¥'.$item_total.'<small class="opos-full-width">($0.66)</small></td>
				// <td data-label="Actions">
					// <button type="button" data-id="'.$key_index.'" class="opos-edit-item m-b-xs btn btn-sm btn-block btn-outline btn-success __editLineItem">Edit</button>
					// <button type="button" data-id="'.$key_index.'" class="opos-delete-item m-b-xs btn btn-sm btn-block btn-outline btn-danger __deleteLineItem">Remove</button>
				// </td>
			// </tr>';
		// }
		// $html_summary='';
		
		// if(!empty($_SESSION['opos_latest_cart_items'])){
			// foreach($_SESSION['opos_latest_cart_items'] as $summary_key => $summary_value){
				// $html_summary.='<div class="row">
					// <div class="col-xs-6 col-md-6">'.$summary_key.' :</div>
					// <div class="col-xs-6 col-md-6 opos-item-total">¥'.$summary_value.'</div>
				// </div>';
			// }
		// }
		$opos_cart_page_id=get_option('opos_cart_page_id');
		$opos_checkout_page_id=get_option('opos_checkout_page_id');
		//$response_arr['data']=$html;
		//$response_arr['html_summary']=$html_summary;
		$response_arr['link']=get_permalink($opos_cart_page_id);
		$response_arr['status']=true;
	}
    echo json_encode($response_arr);
    wp_die();

}

/*
@@ Delete Items from cart
@@ When user will click on submit button
*/

add_action( 'wp_ajax_nopriv_opos_delete_cart', 'opos_delete_cart_options_setting_data' );
add_action( 'wp_ajax_opos_delete_cart', 'opos_delete_cart_options_setting_data' );
function opos_delete_cart_options_setting_data(){
	$form_data=$_REQUEST;
	if(!empty($form_data)){
		unset($_SESSION['opos_cart_items'][$form_data['item_id']]);
	}
	die;
}