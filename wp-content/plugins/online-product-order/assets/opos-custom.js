jQuery( document ).ready( function( $ ) {
	
	function opos_validation(current_obj){
		
		jQuery('.form-group').removeClass('has-error');
		jQuery('.help-block').remove();
		
		var status_check = true;
		jQuery('.'+current_obj+' .opos-form-control:visible').each(function(){

	
		if (jQuery(this).prop('required') && jQuery(this).val() === ""){
			
			var label_text=jQuery(this).parent().find('label').text();
			var label_text=(label_text!=='' ? label_text+' is Required' : 'This Field is Required');
			jQuery(this).parents('.form-group').addClass('has-error');
			if (jQuery(this).parent().find('.help-block').length) {
				jQuery(this).parent().find('.help-block').html(label_text);
			} else {
				jQuery(this).parent().append('<div class="help-block">'+label_text+' </div>')
			}
			status_check = false;
		}
		 

		});
		
		return status_check;
	}
	/*********************************************
	* @Show hide tabs code
	**********************************************/

	jQuery('#item_url_step').on('click',function(event ){
		console.log("clicked");
		jQuery('.opos-step-1').addClass('active-opos');
		jQuery('.opos-step-2').removeClass('active-opos');
		jQuery('.opos-step-3').removeClass('active-opos');
	});

	jQuery('.opos-cart-form').on('click', '.opos-next-btn',function(event ){
		event.preventDefault();
		var next_obj=jQuery(this).attr('data-id');
		var next_step_nav=jQuery(this).attr('data-step');
		var current_obj=jQuery(this).attr('data-current');
		var data_move=jQuery(this).attr('data-move');
		var status_check = true;
		var product_url="";
		
		
		
		status_check=opos_validation(current_obj);
		
		if(status_check){
			product_url=jQuery("input[name='product_url']").val();
			jQuery("input[name='product_url']").val(product_url);
			
			if(data_move!=='no'){
				jQuery('.opos-cart-form .opos-steps').addClass('de-active-opos');
				jQuery('.'+next_obj).removeClass('de-active-opos');
				jQuery('.'+next_obj).addClass('active-opos');
				jQuery('.'+next_step_nav).addClass('active');
			}
			
			/*********************************************
			* @Tabs form submissions
			**********************************************/
			var step=jQuery(this).attr('data-current');
			console.log(step);
			if(step == 'opos-step-2'){
				

// jQuery('#step1nextq').on('click',function(event ){
// 		console.log("step 1 ");
// 	});

			//jQuery('#step2addprod').on('click',function(event ){
				    event.preventDefault();
					console.log("Step 2 ");
				    var product_name = jQuery("#product_name").val();    
				    var product_link = jQuery("#product_url").val();    
				    var product_size_color = jQuery("#product_size_color").val();    
				    var product_quantity = jQuery("#product_quantity").val();
				    var product_item_price_japan = jQuery("#product_item_price_japan").val();
				    var prod_thumb_url = jQuery(".thumb_img").prop('src');

				    console.log("thumb_img.."+prod_thumb_url);
				    jQuery.ajax({
				        url : my_ajax_object.ajax_url,
				        type : 'post',
				        data : {
				            action : 'add_product',
				            product_name : product_name,
				            product_size_color : product_size_color,
				            product_quantity : product_quantity,
				            product_price_japan : product_item_price_japan,
				            product_link : product_link,
				            prod_thumb_url : prod_thumb_url
				        },
				        success : function( response ) {
				            //jQuery('.rml_contents').html(response);
				            //alert("success");
				            var site_base = window.location.href;
				            console.log(site_base);
				            window.location.href = site_base+"/opos-cart/";
				            console.log("secess ajax");
				        }
				    });
				  //  jQuery(this).hide();  
					
				//});


				// var form_url = jQuery(this).attr("data-url");
				// var formData = new FormData(jQuery('.opos-cart-form-area')[0]);
			 //  jQuery.ajax({
				
				// type : "post",
				// dataType : "json",
				// url : form_url,
				// data :formData,
				// processData: false,
				// contentType: false,
				// success: function(response) {
				// 	window.location.href = response.link;
				// },
				// error: function (jqXHR, exception) {
				// 	var msg = '';
				// 	if (jqXHR.status === 0) {
				// 		msg = 'Not connect.\n Verify Network.';
				// 	} else if (jqXHR.status == 404) {
				// 		msg = 'Requested page not found. [404]';
				// 	} else if (jqXHR.status == 500) {
				// 		msg = 'Internal Server Error [500].';
				// 	} else if (exception === 'parsererror') {
				// 		msg = 'Requested JSON parse failed.';
				// 	} else if (exception === 'timeout') {
				// 		msg = 'Time out error.';
				// 	} else if (exception === 'abort') {
				// 		msg = 'Ajax request aborted.';
				// 	} else {
				// 		msg = 'Uncaught Error.\n' + jqXHR.responseText;
				// 	}
				// 	alert(msg);
				// },
				
			 //  });   
			}
			
		}
		
		
	});
	
	
	/********************************************************************************
	@@ On trigger remove button item from cart
	**********************************************************************************/
	jQuery('.opos-cart-view-area').on('click','.opos-delete-item',function(){
		var item_id = jQuery(this).attr('data-id');
		jQuery('.opos-cart-view-area .parent-'+item_id).remove();
		jQuery.ajax
		({ 
			url: opos_js_ajax_object.ajaxurl,
			data: {"item_id": item_id,'action' : opos_js_ajax_object.data_delete_cart},
			type: 'post',
			success: function(result)
			{
				jQuery('.opos-cart-view-area .parent-'+item_id).remove();
			},
			error: function (jqXHR, exception) {
					var msg = '';
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
					} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
					} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
					} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
					} else if (exception === 'timeout') {
						msg = 'Time out error.';
					} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
					} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					alert(msg);
			},
		});
	});


	/********************************************************************************
	@@ Submit user login , register form,prodifle,shipping
	**********************************************************************************/
	jQuery('.opos-user-form').on('click','.opos-user-form-btn1',function(event){
		event.preventDefault();
		var item_id = jQuery(this).attr('data-id');
		var current_obj = jQuery(this).attr('data-clas');
		var formdata =new FormData(jQuery('#'+item_id)[0]);
		
		var status_check=opos_validation(current_obj);
		if(status_check){
			jQuery.ajax
			({ 
				url: opos_js_ajax_object.ajaxurl,
				data: formdata,
				type: 'post',
				dataType: 'JSON',
				contentType: false,
				cache: false,
				processData: false,
				beforeSend: function(){
					
					jQuery('#'+item_id+' .opos-response-area').html('<span class="opos-progress">Please wait.........</span>');
				},
				success: function(result)
				{
					jQuery('#'+item_id+' .opos-response-area').html(result.message);
					if(result.loggedin)
						location.reload();
					
				},
				error: function (jqXHR, exception) {
						var msg = '';
						if (jqXHR.status === 0) {
							msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
							msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
							msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
							msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
							msg = 'Time out error.';
						} else if (exception === 'abort') {
							msg = 'Ajax request aborted.';
						} else {
							msg = 'Uncaught Error.\n' + jqXHR.responseText;
						}
						jQuery('#'+item_id+' .opos-response-area').html('<span class="opos-errors">'+msg+'</span>');
					
				},
			});
		}
	});





 
});