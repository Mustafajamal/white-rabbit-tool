<?php get_header(); ?>
<?php 
	$opos_cart_page_id=get_option('opos_cart_page_id');
	$opos_checkout_page_id=get_option('opos_checkout_page_id');
	$opso_add_cart_page=get_option('opso_add_cart_page');
	$total_checkout=0;
?>
<div class="opos-full-width opos-cart-full">

	<?php if(is_user_logged_in()): ?> 
		<?php //include(plugin_dir_path( __FILE__ ) . 'include/opos-sidebar.php'); ?>
	<?php endif; ?>
	
	<div class="opos-content-area opos-full-width">
		<div  class="opos-full-width opos-cart-form">
			<ul id="opos-cart-progressbar">
				<li class="active step-li-1"><a id="item_url_step" href="">Item URL</a></li>
				<li class="<?= (!empty($_SESSION['opos_cart_items']) ? 'active': 'active'); ?> step-li-2"><a href="#">Item Details</a></li>
				<li class="<?= (!empty($_SESSION['opos_cart_items']) ? 'active': 'active'); ?> step-li-3"><a href="#">My Cart</a></li>
				<li class="step-li-4"><a href="#">Checkout</a></li>
			</ul>
			<!--Display add to Cart Form--->
			
			<?php //include(plugin_dir_path( __FILE__ ) . 'include/opos-cart-form.php'); ?>
			
		</div>
		
		<?php //if(!empty($_SESSION['opos_cart_items'])){ ?>
			<!--Display all current cart added item -->
			<?php //include(plugin_dir_path( __FILE__ ) . 'include/opos-cart-display.php'); ?>
			
		
		<?php //} ?>
		<div class="cart-checkout-footer opos-full-width opos-flex-cart">
			
			<?php echo do_shortcode('[woocommerce_cart]'); ?>
		</div>
	</div>
	
	
</div>
<?php get_footer(); ?>