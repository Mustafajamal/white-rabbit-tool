<h3 class="opos-main-title">You must be logged in to proceed with the checkout process. </h3>
	<div class="opos-full-width opos-user-form opos-cart-form ">
		
		<div class="opos-form-inner">
		
			<h3>Login</h3>
			<form name="login_form" id="opos_login_form" class="login_form" action="" method="post">
				<div class="opos-form-group form-group">
					<label class="opos-full-width">Username <span class="error">*</span></label>  
					<input type="text" name="username" class="opos-form-control" placeholder="Username" required />
				</div>
				<div class="opos-form-group form-group">
					<label class="opos-full-width">Password <span class="error">*</span></label>
					<input type="password" name="password" class="opos-form-control" placeholder="Password" required />
				</div>
				<div class="opos-form-group form-group">
					<input type="hidden" name="action" value="ops_user_login">
					<button name="submit" class="opos-btn-success opos-user-form-btn1" data-clas="login_form" data-id="opos_login_form"><?php _e("Sign in", "shorti"); ?></button>
					<a class="lost" href="<?php echo wp_lostpassword_url(); ?>">Lost your password?</a>

					<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
				</div>
				<div class="opos-response-area"></div>
			</form>
			
		</div>
		<div class="opos-form-inner">
			<h3>Create your account</h3>
			<form action="" method="post" name="user_registeration" class="register_form" id="opos_regiter_form">
				<div class="opos-form-group form-group">
					<label class="opos-full-width">Username <span class="error">*</span></label>  
					<input type="text" name="username" placeholder="Enter Your Username" class="opos-full-width opos-form-control" required />
				</div>	
				<div class="opos-form-group form-group">
					<label class="opos-full-width">Email address <span class="error">*</span></label>
					<input type="text" name="useremail" class="opos-full-width opos-form-control" placeholder="Enter Your Email" required />
				</div>
				<div class="opos-form-group form-group">
					<label class="opos-full-width">Password <span class="error">*</span></label>
					<input type="password" name="password" class="opos-full-width opos-form-control" placeholder="Enter Your password" required />
				</div>
				<div class="opos-form-group">
					<input type="hidden" name="action" value="ops_user_register">
					<input type="submit" data-id="opos_regiter_form" data-clas="register_form"  class="opos-btn-success opos-user-form-btn1" name="user_registeration" value="SignUp" />
				</div>
				<div class="opos-response-area"></div>
			</form>
			
		</div>
	
	</div>
	