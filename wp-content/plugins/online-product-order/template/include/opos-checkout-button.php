<div class="opos-checkout-box">
	<div class="opos-checkout-total">
		<span class="qouts-title">Quote Total:</span>
		<span class="qouts-total">¥<?= $total_checkout; ?></span>
	</div>
	<div class="opos-full-width cart-checkout-page-link">
		<a href="<?= get_permalink($opos_checkout_page_id); ?>" class="opos-btn-warning">Checkout</a>
	</div>
</div>