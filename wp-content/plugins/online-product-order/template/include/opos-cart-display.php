<div class="table-collapse opos-cart-view-area">
		<table cellpadding="1" cellspacing="1" border="1" class="table table-bordered table-striped" bordercolor="#dddddd">
			<thead>
				<tr>
					<th>Item</th>
					<th>Description</th>
					<th>Image</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Total</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<!-- line items -->
				<?php 
		
				if(!empty($_SESSION['opos_cart_items'])){
					
					foreach($_SESSION['opos_cart_items'] as $key_index => $single_item){
						$item_total=$single_item['product_item_price_japan']*$single_item['product_quantity'];
						$total_checkout=$total_checkout+$item_total;
						$html.='
						<tr class="parent-'.$key_index.'">
							<td data-val="'.$single_item['product_url'].'" data-field="product_url">'.$single_item['product_url'].'</td>
							<td data-val="'.$single_item['product_size_color'].'" data-field="product_size_color">'.$single_item['product_size_color'].'</td>
							<td><div class="wre-product-view-cart-image-preview"><span class="font-trans">No image found.</span></div></td>
							<td data-val="'.$single_item['product_quantity'].'" data-field="product_quantity">'.$single_item['product_quantity'].'</td>
							<td data-val="'.$single_item['product_item_price_japan'].'" data-field="product_item_price_japan">¥'.$single_item['product_item_price_japan'].'<small class="opos-full-width">($0.11)</small></td>
							<td data-val="'.$item_total.'" data-field="item_total">¥'.$item_total.'<small class="opos-full-width">($0.66)</small></td>
							<td data-label="Actions">
								<button type="button" data-id="'.$key_index.'" class="opos-edit-item m-b-xs btn btn-sm btn-block btn-outline btn-success __editLineItem">Edit</button>
								<button type="button" data-id="'.$key_index.'" class="opos-delete-item m-b-xs btn btn-sm btn-block btn-outline btn-danger __deleteLineItem">Remove</button>
							</td>
						</tr>';
					}
					echo $html;
				
				}
				?>
				
				
			</tbody>
		</table>
		
	</div>