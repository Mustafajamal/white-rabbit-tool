<?php 
	$opos_cart_page_id=get_option('opos_cart_page_id');
	$opos_checkout_page_id=get_option('opos_checkout_page_id');
	$opso_add_cart_page=get_option('opso_add_cart_page');
	$opos_user_shipping_page_id=get_option('opos_user_shipping_page_id');
	$opos_user_password_page_id=get_option('opos_user_password_page_id');
	$opos_user_profile_page_id=get_option('opos_user_profile_page_id');

?>
<div class="navigation opos-sidebr-area">
    <div class="profile-picture">
        <div class="stats-label text-color">
            <span class="font-extra-bold">test</span>
            <br />
        </div>
    </div>
    <ul class="nav" id="side-menu">
        <li class="">
            <a href="/dashboard"> <span class="nav-label">Dashboard</span></a>
        </li>
        <li class="active">
            <a href="<?= get_permalink($opso_add_cart_page); ?>"><span class="nav-label">New Request</span></a>
        </li>
        <li class="">
            <a href="<?= get_permalink($opos_cart_page_id); ?>"> <span class="nav-label">Your Cart</span></a>
        </li>
        <li class="">
            <a href="/orders"> <span class="nav-label">Orders</span> </a>
        </li>
       
        <li class="">
            <a href="#"> <span class="nav-label">Shipments</span> </a>
        </li>
      
        <li class="active">
          
			<li class="">
				<a href="<?= get_permalink($opos_user_profile_page_id); ?>">Profile</a>
			</li>
			<li class="">
				<a href="<?= get_permalink($opos_user_shipping_page_id); ?>">Shipping Address</a>
			</li>
			<li class="">
				<a href="<?= get_permalink($opos_user_password_page_id); ?>">Password</a>
			</li>
          
       
    </ul>
</div>
