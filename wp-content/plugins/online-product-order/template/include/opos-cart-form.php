<form class="opos-full-width opos-cart-form-area">

	
	<!--Steps form 3-->
	<div class="opos-full-width opos-step-3 opos-steps <?= (!empty($_SESSION['opos_cart_items']) ? 'active-opos': 'de-active-opos'); ?>">
		<div class="panel-heading hbuilt">
			<span class="">My Cart - Summary.</span>
		</div>
		<div class="opos-full-width opos-panel-area">
	
			<?php if(!empty($_SESSION['opos_latest_cart_items'])){ ?>
				<div class="panel-body summary-details">
					<div class="opos-full-width opos_cart_summaryy">
						<?php 
							
							if(!empty($_SESSION['opos_latest_cart_items'])){
								foreach($_SESSION['opos_latest_cart_items'] as $summary_key => $summary_value){
									echo '<div class="row">
										<div class="col-xs-6 col-md-6">'.$summary_key.' :</div>
										<div class="col-xs-6 col-md-6 opos-item-total">¥'.$summary_value.'</div>
									</div>';
								}
							}
						?>
					</div>

			
					
					
					<hr />
					<h4 class="bold-title">Get your items faster with Domestic Shipping Pre-Approval</h4>

					<div class="form-group footer-approval-section" data-required="true">
						<div class="af-radio-group">
							<div class="radio">
								<span class="label label-primary2 recommended-label opos-full-width">
									<input type="radio" name="approval_checkbox" value="primary" checked> I pre-approve up to $9.63 per-shop for domestic shipping.<span class="label label-primary25 recommended-label">Recommended</span>
								</span>
								<span class="label label-primary2 recommended-label opos-full-width">
									<input type="radio" name="approval_checkbox" value="recommended">I want to manually approve any shipping charges (may delay your order).
								</span>
							</div>

							
						</div>

						<span class="help-block"></span>
					</div>

					
				</div>
			<?php } ?>

		</div>
		<div class="panel-footer opos-full-width">
			Next, we’ll gather a few more details so we can make a quote for you.
		</div>
	</div>
	
	
</form>
