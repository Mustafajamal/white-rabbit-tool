<div class="add-cart-btn">
	<a href="<?= get_permalink($opso_add_cart_page); ?>" class="btn btn-labeled btn-flat opos-btn-success">
			<span class="btn-label icon fa fa-plus"></span> Add another item
   </a>
</div>
		