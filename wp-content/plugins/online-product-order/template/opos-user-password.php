<?php 
if ( !is_user_logged_in()) {

	wp_redirect(home_url()); 
	exit;
}
?>
<?php get_header(); ?>
<?php 
	$opos_cart_page_id=get_option('opos_cart_page_id');
	$opos_checkout_page_id=get_option('opos_checkout_page_id');
	$opso_add_cart_page=get_option('opso_add_cart_page');
	$total_checkout=0;
	$user = wp_get_current_user();

?>
<div class="opos-full-width opos-cart-full">

	<?php if(is_user_logged_in()): ?> 
		<?php include(plugin_dir_path( __FILE__ ) . 'include/opos-sidebar.php'); ?>
	<?php endif; ?>
	
	<div class="<?= (is_user_logged_in()) ? 'opos-content-area': 'opos-full-width';?>">
		<h3 class="opos-main-title">Update Password </h3>
		<div class="opos-full-width opos-user-form opos-cart-form ">
			
			<div class="opos-form-inner">
				<form action="" method="post" class="opos_user_password" id="opos_user_password">
					<div class="opos-form-group form-group">
						<label class="opos-full-width">Current Password <span class="error">*</span></label>  
						<input type="password" name="current_password" placeholder="Enter Current Password" class="opos-full-width opos-form-control"required />
					</div>	
					<div class="opos-form-group form-group">
						<label class="opos-full-width">New Password <span class="error">*</span></label>
						<input type="password" name="new_password"  class="opos-full-width opos-form-control" placeholder="New Password " required />
					</div>
					<div class="opos-form-group form-group">
						<label class="opos-full-width">Confirm Password</label>
						<input type="password" name="confirm_password" class="opos-full-width opos-form-control" placeholder="Confirm Password"  required />
					</div>
					
					<div class="opos-form-group">
						<input type="hidden" name="action" value="ops_user_password_update">
						<input type="submit" data-id="opos_user_password" data-clas="opos_user_password"  class="opos-btn-success opos-user-form-btn1" name="user_registeration" value="Update" />
					</div>
					<div class="opos-response-area"></div>
				</form>
				
			</div>
		
		</div>
		
	</div>
	
	
</div>
<?php get_footer(); ?>