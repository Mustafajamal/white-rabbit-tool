<?php 
if ( !is_user_logged_in()) {

	wp_redirect(home_url()); 
	exit;
}
?>
<?php get_header(); ?>
<?php 
	$opos_cart_page_id=get_option('opos_cart_page_id');
	$opos_checkout_page_id=get_option('opos_checkout_page_id');
	$opso_add_cart_page=get_option('opso_add_cart_page');
	$total_checkout=0;
	$user = wp_get_current_user();

?>
<div class="opos-full-width opos-cart-full">

	<?php if(is_user_logged_in()): ?> 
		<?php include(plugin_dir_path( __FILE__ ) . 'include/opos-sidebar.php'); ?>
	<?php endif; ?>
	
	<div class="<?= (is_user_logged_in()) ? 'opos-content-area': 'opos-full-width';?>">
		<h3 class="opos-main-title">User Profile </h3>
		<div class="opos-full-width opos-user-form opos-cart-form ">
			
			<div class="opos-form-inner">
				<form action="" method="post" class="opos_user_profiler" id="opos_user_profile">
					<div class="opos-form-group form-group">
						<label class="opos-full-width">Username <span class="error">*</span></label>  
						<input type="text" name="username" placeholder="Enter Your Username" value="<?= (!empty($user->data->user_login)) ? $user->data->user_login : '';?>" class="opos-full-width opos-form-control" readonly />
					</div>	
					<div class="opos-form-group form-group">
						<label class="opos-full-width">Email address <span class="error">*</span></label>
						<input type="text" name="useremail" value="<?= (!empty($user->data->user_email)) ? $user->data->user_email : '';?>" class="opos-full-width opos-form-control" placeholder="Enter Your Email" required />
					</div>
						<div class="opos-form-group form-group">
						<label class="opos-full-width">Websit URL</label>
						<input type="text" name="userwebsite" value="<?= (!empty($user->data->user_url)) ? $user->data->user_url : '';?>" class="opos-full-width opos-form-control" placeholder="EnterWebsit URL"  />
					</div>
					
					<div class="opos-form-group">
						<input type="hidden" name="action" value="ops_user_profile_update">
						<input type="submit" data-id="opos_user_profile" data-clas="opos_user_profiler"  class="opos-btn-success opos-user-form-btn1" name="user_registeration" value="Update" />
					</div>
					<div class="opos-response-area"></div>
				</form>
				
			</div>
		
		</div>
		
	</div>
	
	
</div>
<?php get_footer(); ?>