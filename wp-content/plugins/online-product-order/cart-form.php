<?php 
add_shortcode('opos_show_cart_form','opos_shortcode_function');
function opos_shortcode_function(){
	
	global $wp_session;
	global $post;
	update_option('opso_add_cart_page', $post->ID);
	
	
	$opos_cart_page_id=get_option('opos_cart_page_id');
	$opos_checkout_page_id=get_option('opos_checkout_page_id');
	$opso_add_cart_page=get_option('opso_add_cart_page');
	$total_checkout=0;
?>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script type="text/javascript">
		jQuery(document).ready(function () {
		    
		    jQuery('.first_step_next_btn').prop('disabled', true);
		    jQuery("#not_valid_url").hide();
		    jQuery('#product_url').keyup(function() {
		        var $th = jQuery(this);
		        
		        if (isValidUrl($th.val())==1){
		            //alert("got a valid url!");
					jQuery("#not_valid_url").hide();
					jQuery('.first_step_next_btn').prop('disabled', false);

		        }else{
		        	jQuery("#not_valid_url").show();
		        }
		     
		    });
		});


		function isValidUrl(url){

		 var myVariable = url;
		    if(/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(myVariable)) {
		      return 1;
		    } else {
		      return -1;
		    }   
		}
</script>
<div class="opos-full-width">
	
	<?php //if(is_user_logged_in()): ?> 
		<?php // include(plugin_dir_path( __FILE__ ) . 'template/include/opos-sidebar.php'); ?>
	<?php //endif; ?>
	
	<div class="opos-content-area opos-full-width opos-cart-form">
		<ul id="opos-cart-progressbar">
			<li class="active step-li-1"><a id="item_url_step" href="#">Item URL</a></li>
			<li class="step-li-2"><a id="item_detail_step" href="">Item Details</a></li>
			<li class="step-li-3"><a href="<?= get_permalink($opos_cart_page_id); ?>"> <span class="nav-label">My Cart</span></a></li>
			<li class="step-li-4"><a href="#">Checkout</a></li>
		</ul>

		<form class="opos-full-width opos-cart-form-area">
			<!---Steps One -->
			<div class="opos-full-width opos-step-1 opos-steps active-opos">
				<div class="panel-heading hbuilt">
					<span class="">Add a URL for the product you want us to buy.</span>
				</div>

				<div class="opos-full-width opos-panel-area">
					<div class="form-group opos-full-width">
						<label class="opos-full-width">Product web page</label>
						<input type="url" class="opos-full-width opos-form-control" id="product_url" name="product_url" placeholder="http://" required>
						<span id="not_valid_url" style="color: red;">Not a Valid URL!</span>
						<div class="Product_thumb"><?php echo do_shortcode('[ajax-file-upload unique_identifier=prod_thumb on_success_set_input_value="#prod_thumb_url" set_image_source="img.thumb_img" max_size=5000]');?></div>
						<div class="uploaded_image_thumb"><img style="width:200px;" class="thumb_img" src=""></div>
						<input type="hidden" id="prod_thumb_url" value="" name="prod_thumb_url">
					</div>
					<div class="opos-steps-btn opos-full-width">
						<button type="button" class="first_step_next_btn opos-next-btn opos-btn-success" data-id="opos-step-2" data-current="opos-step-1" data-step="step-li-2">Next</button>
					</div>
				</div>
				<div class="panel-footer opos-full-width">
					Next, we’ll gather a few more details so we can make a quote for you.
				</div>
			</div>
			
			<!--Steps form 2-->
			<div class="opos-full-width opos-step-2 opos-steps de-active-opos">
				<div class="panel-heading hbuilt">
					<span class="">We need a few more details to create your quote.</span>
				</div>
				<div class="opos-full-width opos-panel-area">
					<div class="form-group opos-full-width">
						<label class="opos-full-width">Product web page</label>
						<input type="url" class="opos-full-width opos-form-control" id="product_url" name="product_url" placeholder="http://">
					</div>
					<div class="form-group opos-full-width">
						<label class="opos-full-width">Product name</label>
						<input type="text" class="opos-full-width opos-form-control" id="product_name" name="product_name" required>
					
					</div>
					<div class="form-group opos-full-width">
						<label class="opos-full-width">Size, color, description</label>
						<input type="text" class="opos-full-width opos-form-control" id="product_size_color" name="product_size_color">
					
					</div>
					<div class="form-group opos-full-width">
						<label class="opos-full-width">Quantity to buy</label> 
						<input type="number" class="opos-full-width opos-form-control" id="product_quantity" name="product_quantity" required>
					
					</div>
					<div class="form-group opos-full-width">
						<label class="opos-full-width">Item price in Japanese Yen</label>
						<input type="number" class="opos-full-width opos-form-control" value="" id="product_item_price_japan" name="product_item_price_japan" required>
					</div>
					
					<div class="opos-steps-btn opos-full-width">
						<button type="button" id="step2addprod" class="opos-next-btn opos-btn-success" data-id="opos-step-3" data-current="opos-step-2" data-step="step-li-3">Add Item to Cart</button>
					</div>
				</div>
				<div class="panel-footer opos-full-width">
					Next, we’ll gather a few more details so we can make a quote for you.
				</div>
			</div>
			<!--Steps form 3-->
			<div class="opos-full-width opos-step-3 opos-steps de-active-opos">
				<div class="panel-heading hbuilt">
					<span class="">Cart</span>
				</div>
				<div class="opos-full-width opos-panel-area">
					
					
					<div class="form-group opos-full-width">
						<?php echo do_shortcode('[woocommerce_cart]'); ?>
					</div>
					
					<div style="display: none;" class="opos-steps-btn opos-full-width">
						<button type="button" id="step2addprod" class="opos-next-btn opos-btn-success ajax-add-cart-btn" data-id="opos-step-4" data-move="no" data-current="opos-step-3" data-step="step-li-3" data-url="">Checkout</button>
					</div>
				</div>
				<div class="panel-footer opos-full-width">
					Next, we’ll gather a few more details so we can make a quote for you.
				</div>
			</div>
			

			
			
		</form>
	
		
	</div>
	

	
	
	

</div>


<?php	
}