<?php
/**
 * Plugin Name:  Logics BufferOnline Products Order System
 * Plugin URI:  http://logicsbuffer.com/      
 * Description:   Use this plugin to purchase product by using url
 * Version:           1.10.3
 * Requires PHP:      7.2
 * Author:            Mustafa Jamal
 * Author URI:       http://logicsbuffer.com/ 
 * License:           GPL v2 or later       
 * Domain Path:       /languages
 */
 
 

define( 'OPOS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
/*************************************************
 @@ Register Admin Script 
***************************************************/
function opos_register_admin_script($hook) {
	wp_enqueue_style('opos_css', plugins_url('assets/opos-admin.css',__FILE__ ));
	$opos_js  = date("opos_js", filemtime( plugin_dir_path( __FILE__ ) . 'assets/opos-custom.js' ));
	wp_enqueue_script( 'opos_js', plugins_url( 'assets/opos-custom.js?v='.rand(0,1000), __FILE__ ), array( 'jquery' ), $opos_js );
	wp_localize_script( 'opos_js', 'opos_js_ajax_object',
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'data_delete_cart' => 'opos_delete_cart',
            'data_edit_cart' => 'opos_edit_cart',
        )
    );	
  //wp_localize_script( 'rml-script', 'readmelater_ajax', array( 'ajax_url' => admin_url('admin-ajax.php')) ); 

}

function my_enqueue() {

    wp_enqueue_script( 'ajax-script', plugins_url(''), array('jquery') );

    wp_localize_script( 'ajax-script', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue' );
//add_action( 'wp_ajax_add_product', 'read_me_later' );
add_action( 'wp_ajax_nopriv_add_product', 'read_me_later' );

function read_me_later() {
          
     $product_name = $_REQUEST['product_name'];
     $product_size_color = $_REQUEST['product_size_color'];
     $product_quantity = $_REQUEST['product_quantity'];
     $product_price = $_REQUEST['product_price_japan'];
     $product_link = $_REQUEST['product_link'];
     $prod_thumb_url = $_REQUEST['prod_thumb_url'];

     $product_id = wp_insert_post(array(
      'post_title' => $product_name,
      'post_excerpt' => $product_link,
      'post_type' => 'product',
      'post_staus' => 'publish'
    ));
    wp_set_object_terms( $product_id, 'simple', 'product_type' );
    update_post_meta( $product_id, '_visibility', 'visible' );
    update_post_meta( $product_id, '_regular_price', $product_price );
    update_post_meta( $product_id, '_price', $product_price );
    update_post_meta( $product_id, '_stock', $product_quantity );
    update_post_meta( $product_id, '_sku', $product_size_color );

    wp_update_post( array(
      'ID' => $product_id,
      'post_status' => 'publish',
    ));
    
    //Set a fields to specific values
    update_field('product_url', $product_link, $product_id);
    update_field('size_color_desc', $product_size_color, $product_id);
    update_field('quantity_to_buy', $product_quantity, $product_id);
    update_field('product_thumbnail_url', $prod_thumb_url, $product_id);
    
    
    //Custom Product Image
    $image_url        = $_REQUEST['prod_thumb_url']; // Define the image URL here
    $image_name       = 'prod-image-'.$product_id.'.png';
    $upload_dir       = wp_upload_dir(); // Set upload folder
    $image_data       = file_get_contents($image_url); // Get image data
    $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
    $filename         = basename( $unique_file_name ); // Create image file name

    // Check folder permission and define file location
    if( wp_mkdir_p( $upload_dir['path'] ) ) {
        $file = $upload_dir['path'] . '/' . $filename;
    } else {
        $file = $upload_dir['basedir'] . '/' . $filename;
    }

    // Create the image  file on the server
    file_put_contents( $file, $image_data );

    // Check image file type
    $wp_filetype = wp_check_filetype( $filename, null );

    // Set attachment data
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title'     => sanitize_file_name( $filename ),
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    // Create the attachment
    $attach_id = wp_insert_attachment( $attachment, $file, $product_id );

    // Include image.php
    require_once(ABSPATH . 'wp-admin/includes/image.php');

    // Define attachment metadata
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

    // Assign metadata to attachment
    wp_update_attachment_metadata( $attach_id, $attach_data );

    // And finally assign featured image to post
    set_post_thumbnail( $product_id, $attach_id );
    //global $woocommerce;

    //$woocommerce->cart->add_to_cart( $product_id );
    //WC()->cart->empty_cart(); 
    WC()->cart->add_to_cart($product_id);
}

//Simple Ajax for Login
add_action( 'wp_ajax_add_product', 'read_me_later_login' );

function read_me_later_login() {
          
     $product_name = $_REQUEST['product_name'];
     $product_size_color = $_REQUEST['product_size_color'];
     $product_quantity = $_REQUEST['product_quantity'];
     $product_price = $_REQUEST['product_price_japan'];
     $product_link = $_REQUEST['product_link'];
     $prod_thumb_url = $_REQUEST['prod_thumb_url'];

     $product_id = wp_insert_post(array(
      'post_title' => $product_name,
      'post_excerpt' => $product_link,
      'post_type' => 'product',
      'post_staus' => 'publish'
    ));
    wp_set_object_terms( $product_id, 'simple', 'product_type' );
    update_post_meta( $product_id, '_visibility', 'visible' );
    update_post_meta( $product_id, '_regular_price', $product_price );
    update_post_meta( $product_id, '_price', $product_price );
    update_post_meta( $product_id, '_stock', $product_quantity );
    update_post_meta( $product_id, '_sku', $product_size_color );

    wp_update_post( array(
      'ID' => $product_id,
      'post_status' => 'publish',
    ));
    //Set a fields to specific values
    update_field('product_url', $product_link, $product_id);
    update_field('size_color_desc', $product_size_color, $product_id);
    update_field('quantity_to_buy', $product_quantity, $product_id);
    update_field('product_thumbnail_url', $prod_thumb_url, $product_id);

    //Custom Product Image
    $image_url        = $_REQUEST['prod_thumb_url']; // Define the image URL here
    $image_name       = 'prod-image-'.$product_id.'.png';
    $upload_dir       = wp_upload_dir(); // Set upload folder
    $image_data       = file_get_contents($image_url); // Get image data
    $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
    $filename         = basename( $unique_file_name ); // Create image file name

    // Check folder permission and define file location
    if( wp_mkdir_p( $upload_dir['path'] ) ) {
        $file = $upload_dir['path'] . '/' . $filename;
    } else {
        $file = $upload_dir['basedir'] . '/' . $filename;
    }

    // Create the image  file on the server
    file_put_contents( $file, $image_data );

    // Check image file type
    $wp_filetype = wp_check_filetype( $filename, null );

    // Set attachment data
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title'     => sanitize_file_name( $filename ),
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    // Create the attachment
    $attach_id = wp_insert_attachment( $attachment, $file, $product_id );

    // Include image.php
    require_once(ABSPATH . 'wp-admin/includes/image.php');

    // Define attachment metadata
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

    // Assign metadata to attachment
    wp_update_attachment_metadata( $attach_id, $attach_data );

    // And finally assign featured image to post
    set_post_thumbnail( $product_id, $attach_id );

    global $woocommerce;
    //$woocommerce->cart->add_to_cart( $product_id );
    //WC()->cart->empty_cart(); 
    WC()->cart->add_to_cart( $product_id );
}

add_action( 'wp_enqueue_scripts', 'opos_register_admin_script' );
function opos_custom_startSession() {
    if(!session_id()) {
        session_start();
    }
}
add_action('init', 'opos_custom_startSession', 1);




/***************************************************************
@@Create page on activate plugin
**********************************************************/
function opos_add_custom_page() {
    
  if ( ! current_user_can( 'activate_plugins' ) ) return;
  
  global $wpdb;
  
  if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'opos-cart'", 'ARRAY_A' ) ) {
     
    $current_user = wp_get_current_user();
    $page = array(
      'post_title'  => __( 'Opos Cart' ),
      'post_status' => 'publish',
      'post_author' => $current_user->ID,
      'post_type'   => 'page',
    );
    
    $cart_page_id=wp_insert_post( $page );
	update_option('opos_cart_page_id',$cart_page_id);
  }
  if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'opos-cart-checkout'", 'ARRAY_A' ) ) {
     
    $current_user = wp_get_current_user();
    $page = array(
      'post_title'  => __( 'Opos Cart Checkout' ),
      'post_status' => 'publish',
      'post_author' => $current_user->ID,
      'post_type'   => 'page',
    );
    
   $checkout_page_id= wp_insert_post( $page );
   update_option('opos_checkout_page_id',$checkout_page_id);
  }
  if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'opos-user-shipping-method'", 'ARRAY_A' ) ) {
     
   //  $current_user = wp_get_current_user();
   //  $page = array(
   //    'post_title'  => __( 'Opos User Shipping' ),
   //    'post_status' => 'publish',
   //    'post_author' => $current_user->ID,
   //    'post_type'   => 'page',
   //  );
    
   // $checkout_page_id= wp_insert_post( $page );
   // update_option('opos_user_shipping_page_id',$checkout_page_id);
  }
  if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'opos-user-password'", 'ARRAY_A' ) ) {
     
   //  $current_user = wp_get_current_user();
   //  $page = array(
   //    'post_title'  => __( 'Opos User Password' ),
   //    'post_status' => 'publish',
   //    'post_author' => $current_user->ID,
   //    'post_type'   => 'page',
   //  );
    
   // $checkout_page_id= wp_insert_post( $page );
   // update_option('opos_user_password_page_id',$checkout_page_id);
  }
  if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'opos-user-profile'", 'ARRAY_A' ) ) {
     
   //  $current_user = wp_get_current_user();
   //  $page = array(
   //    'post_title'  => __( 'Opos User Profile' ),
   //    'post_status' => 'publish',
   //    'post_author' => $current_user->ID,
   //    'post_type'   => 'page',
   //  );
    
   // $checkout_page_id= wp_insert_post( $page );
   // update_option('opos_user_profile_page_id',$checkout_page_id);
  }
}

register_activation_hook(__FILE__, 'opos_add_custom_page');

/*************************************************************
@@Call custom template for specific pages
***************************************************************/
add_filter( 'page_template', 'opos123_page_template' );
function opos123_page_template( $page_template )
{
	$opos_cart_page_id=get_option('opos_cart_page_id');
	$opos_checkout_page_id=get_option('opos_checkout_page_id');
	$opso_add_cart_page=get_option('opso_add_cart_page');
	$opos_user_shipping_page_id=get_option('opos_user_shipping_page_id');
	$opos_user_password_page_id=get_option('opos_user_password_page_id');
	$opos_user_profile_page_id=get_option('opos_user_profile_page_id');
	
	if(!empty($opos_cart_page_id)){
		$opos_cart_page_id = get_post($opos_cart_page_id); 
		$opos_cart_page_slug = $opos_cart_page_id->post_name;
		
		if ( is_page( $opos_cart_page_slug) ) {
			$page_template = dirname( __FILE__ ) . '/template/opos-cart-page.php';
		}
	}
    if(!empty($opos_checkout_page_id)){
		$opos_checkout_page_id = get_post($opos_checkout_page_id); 
		$opos_checkout_page_id = $opos_checkout_page_id->post_name;
		
		if ( is_page( $opos_checkout_page_id) ) {
			$page_template = dirname( __FILE__ ) . '/template/opos-checkout-page.php';
		}
	}
	if(!empty($opos_user_shipping_page_id)){
		$opos_user_shipping_page_id = get_post($opos_user_shipping_page_id); 
		$opos_user_shipping_page_id = $opos_user_shipping_page_id->post_name;
		
		if ( is_page( $opos_user_shipping_page_id) ) {
			$page_template = dirname( __FILE__ ) . '/template/opos-user-shipping.php';
		}
	}
	
	if(!empty($opos_user_password_page_id)){
		$opos_user_password_page_id = get_post($opos_user_password_page_id); 
		$opos_user_password_page_id = $opos_user_password_page_id->post_name;
		
		if ( is_page( $opos_user_password_page_id) ) {
			$page_template = dirname( __FILE__ ) . '/template/opos-user-password.php';
		}
	}
	if(!empty($opos_user_profile_page_id)){
		$opos_user_profile_page_id = get_post($opos_user_profile_page_id); 
		$opos_user_profile_page_id = $opos_user_profile_page_id->post_name;
		
		if ( is_page( $opos_user_profile_page_id) ) {
			$page_template = dirname( __FILE__ ) . '/template/opos-user-profile.php';
		}
	}
	
    return $page_template;
}
include( plugin_dir_path( __FILE__ ) . 'cart-form.php');
include( plugin_dir_path( __FILE__ ) . 'admin/opos-admin-hooks.php');
// include( plugin_dir_path( __FILE__ ) . 'admin/wcz-options.php');
